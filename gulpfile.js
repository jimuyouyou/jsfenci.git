var gulp = require('gulp');
var jsmin = require('gulp-jsmin');
var rename = require('gulp-rename');

function defaultTask(cb) {
  gulp.src('index.js')
    .pipe(jsmin())
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest('.'));

  cb();
}

exports.default = defaultTask