# jsfenci
- 中文纯js分词，浏览器可直接使用
- 支持自定义分词词库
- [在线demo](http://jimuyouyou.gitee.io/jsfenci/)
- [实际项目使用](http://jimuyouyou.gitee.io/i3000men/)

# 项目缘由
- 最近做[i3000men](https://gitee.com/jimuyouyou/i3000men)这个纯净网址导航项目，想引入前端的分词
- 要求非常简单：能快速分词本网站支持的词库即可，不用支持所有中文分词
- 看了下已有库，大多数是后端的，太复杂，还会影响性能。我只想前端快速使用，不调用任何api!
- 于是写了这个框架，包含了主要逻辑，到于词库，用户可以自己在使用时灵活定义！

#### 快速使用
```
<script type="text/javascript" src="http://jimuyouyou.gitee.io/jsfenci/index.min.js"></script>

var myDict = [
  "家乡",
  "松花",
  "松花江",
  "那里",
  "四季",
  "四季迷人",
  "迷人",
  "花香",
  "hello",
  "kitty",
  "fine"
];
var words = 'hello, kitty!我的家乡在松花江边上，那里有四季迷人的花香';

var fenci = new JsFenci();
fenci.init(myDict);
var result = fenci.splitWords(words);
console.log(result);
```

#### 参考资料
- [JavaScript: 实现简单的中文分词](https://my.oschina.net/goal/blog/201674)
- [分割词](https://github.com/leizongmin/node-segment/blob/master/dicts/stopword.txt)
